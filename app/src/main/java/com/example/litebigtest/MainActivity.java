package com.example.litebigtest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import com.example.litebigtest.adapter.AdapterPegawai;
import com.example.litebigtest.model.PlayerData;
import com.example.litebigtest.network.GetDataService;
import com.example.litebigtest.network.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private GetDataService service;
    ProgressDialog progressDialog;
    private List<PlayerData> list;
    private AdapterPegawai adapterPegawai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupView();
        networkRequest();
    }

    private void setupView() {
        recyclerView = findViewById(R.id.recyclerView);
    }

    private void networkRequest() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        service = RetrofitClient.getRetrofitInstance().create(GetDataService.class);
        service.getData().enqueue(new Callback<List<PlayerData>>(){
            @Override
            public void onResponse(Call<List<PlayerData>> call, Response<List<PlayerData>> response) {
                progressDialog.dismiss();

                if (response.body() != null){
                    list = response.body();
                    showRecyclerView(list);
                }
            }

            @Override
            public void onFailure(Call<List<PlayerData>> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Connection Problem", Toast.LENGTH_SHORT).show();
            }

        });
    }
    private void showRecyclerView(List<PlayerData> list) {
        recyclerView.setLayoutManager(new LinearLayoutManager(
                this,
                RecyclerView.VERTICAL,
                false
        ));
        adapterPegawai = new AdapterPegawai(this, list);
        recyclerView.setAdapter(adapterPegawai);
    }
}
