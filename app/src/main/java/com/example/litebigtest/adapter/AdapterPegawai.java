package com.example.litebigtest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.litebigtest.R;
import com.example.litebigtest.model.PlayerData;

import java.util.List;

public class AdapterPegawai extends RecyclerView.Adapter<AdapterPegawai.ViewHolder> {

    private Context context;
    private List<PlayerData> list;

    public AdapterPegawai(Context context, List<PlayerData> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final PlayerData data = list.get(position);
        holder.id.setText(data.getId());
        holder.nip.setText(data.getNip());
        holder.nama.setText(data.getNama());
        holder.alamat.setText(data.getAlamat());
        holder.noTelp.setText(data.getNoTelp());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView id, nip, nama, alamat, noTelp;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.tvid);
            nip = itemView.findViewById(R.id.tvnip);
            nama = itemView.findViewById(R.id.tvnama);
            alamat = itemView.findViewById(R.id.tvalamat);
            noTelp = itemView.findViewById(R.id.tvnotelp);
        }
    }
}
