package com.example.litebigtest.network;

import com.example.litebigtest.model.PlayerData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetDataService {
    @GET("select_pegawai")
    Call<List<PlayerData>> getData();
}
